var timer;
export function debounce(func, timeout = 300) {
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            timer = undefined;
            func.apply(this, args);
        }, timeout);
    };
}
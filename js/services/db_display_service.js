import { HippoDbServices } from './hippo_db_service.js';

const DbDisplayServices = {
    fetchSearchWithField,
    fetchRecords,
    fetchTotalRows,
    fetchColumns,
    fetchSelectAll,
    fetchListTable,
}

export default DbDisplayServices;

function fetchSearchWithField(table, field_name, keyword, limit, offset) {
    return HippoDbServices.searchSubstring(table, field_name, keyword, limit, offset);
}

function fetchRecords(table, field_name, list_country, limit, offset) {
    return HippoDbServices.whereIn(table, field_name, list_country, limit, offset);
}

function fetchTotalRows(table) {
    return HippoDbServices.totalRows(table);
}

function fetchColumns(table) {
    HippoDbServices.getTableInfo(table);
    return HippoDbServices.getTableInfo(table);
}

function fetchSelectAll(table, limit, offset) {
    return HippoDbServices.selectAll(table, limit, offset);
}

function fetchListTable(){
    return HippoDbServices.listAllTable();
}

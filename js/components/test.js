import DbDisplayServices from '../services/db_display_service.js';
import { HippoDbServices } from "../services/hippo_db_service.js";

// let template = "";
// const DEV_MODE = false;

// const handleClickLoadTable = function (e) {
//     let tableName = this.getAttribute('data-table-name');
//     $('.table-name-input')[0].value = tableName;
//     $('#modalListTable').modal('hide');
//     $('.table-name-input-button')[0].click();
// }

// const getTemplateListItemTable = function () {
//     if (template != "") return template;
//     return $.get("modal_table.html", function (data) {
//         template = data;
//         return template;
//     });
// }

// const renderListItemTable = function (table, template) {
//     template.querySelector(".grid-item-table__table-name").innerHTML = table.table_name;
//     template.querySelector(".grid-item-table__content").innerHTML = "<a href='#' class='grid-item-table__content-link'>" + table.url + "</a>";
//     template.querySelector(".grid-item-table__duration-value").innerHTML = table.duration;
//     template.querySelector(".grid-item-table__action--load").setAttribute('data-table-name', table.table_name);
//     template.querySelector(".grid-item-table__action--load").addEventListener('click', handleClickLoadTable);
//     const aTag = template.querySelector('.grid-item-table__content-link');
//     const rootUrl = 'https://docs.google.com/spreadsheets/d/';
//     aTag.href = rootUrl + table.url;
//     aTag.target = '_blank';
//     return template;
// }

// const renderListTable = async (listTable) => {
//     if (!listTable) return;
//     let n_records = listTable.length;
//     let n_records_txt = n_records === 0 ? "No records" : n_records + " record" + (n_records > 1 ? "s" : "");
//     $(".modal-body__header-left-number-of-records")[0].innerHTML = n_records_txt;
//     $('#modalListTable .modal-body .grid-list-table').empty();
//     let template = await getTemplateListItemTable();
//     let templateDom = $(template)[0];
//     listTable.forEach(function (table) {
//         console.log(1)
//         let templateClone = templateDom.cloneNode(true);
//         let listItemDom = renderListItemTable(table, templateClone);
//         $('#modalListTable .modal-body .grid-list-table').append(listItemDom);
//     });
// }

const fetchListAllTables = function () {
    HippoDbServices.listAllTable()
        .then(function (response) {
            let listTable = response;
            console.log(listTable);
            renderListTable(listTable);
        })
        .catch(function (error) {
            console.log(error);
        });
}
// //
// const isSuccessCreateTable = function (response) {
//     let nKey = Object.keys(response).length;
//     if (nKey <= 1) return false;
//     if (response.status === 'error') return false;
//     return true;
// }

// const fetchCreateTable = function (tableName, tableUrl) {
//     return HippoDbServices.createTable(tableName, tableUrl)
//         .then(function (response) {
//             if (!isSuccessCreateTable(response)) {
//                 updateTextAlertAddTable('danger', response.table_name);
//                 return false;
//             }
//             updateTextAlertAddTable('success', 'Table created successfully');
//         })
//         .catch(function (error) {
//             updateTextAlertAddTable('danger', 'Error creating table');
//         });
// }

// const updateTextAlertAddTable = function (type, message) {
//     let alert = $('#addTable .alert');
//     alert.removeClass('d-none');
//     alert.removeClass('alert-success');
//     alert.removeClass('alert-danger');
//     alert.addClass('alert-' + type);
//     alert.html(message);
// }


// const handleDevMode = function () {
//     const tableList =
//     [
//         {
//             "table_name": "test",
//             "url": "hbs74OgvE2upms/edit#gid=0",
//             "time_built": "2022-01-05T02:48:16.051282Z",
//             "duration": 2,
//             "created_at": "2022-01-05T02:48:16.051282Z",
//             "updated_at": "2022-01-05T02:48:16.051282Z"
//         },
//         {
//             "table_name": "test2",
//             "url": "hRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit#gid=0",
//             "time_built": "2022-01-05T02:48:49.800220Z",
//             "duration": 2,
//             "created_at": "2022-01-05T02:48:49.800220Z",
//             "updated_at": "2022-01-05T02:48:49.800220Z"
//         }
//     ];

//     $('#modalListTable').modal('show');
//     renderListTable(tableList);
    
//     $(".btn-add-table-submit")[0].addEventListener('click', function () {
//         fetchCreateTable('test', 'hbs74OgvE2upms/edit#gid=0')
//     });
// }

// const handleProductionMode = function () {
//     fetchListAllTables();
//     $('#modalListTable .btn-refresh').click(function () {
//         fetchListAllTables();
//     });
//     $('.btn-add-table-submit')[0].addEventListener('click', function () {
//         let tableName = $('#addTable #tableName').val();
//         let tableUrl = $('#addTable #tableUrl').val();
//         fetchCreateTable(tableName, tableUrl);
//     });
// }


// if (DEV_MODE) {
//     handleDevMode();
// } else {
//     handleProductionMode();
// }
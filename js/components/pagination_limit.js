import DbDisplayConstants from "../constants/db_display_constants.js";
import PaginationServices from "../constants/paginations.js";

export default class PaginationLimit {
    constructor(initialLimit = DbDisplayConstants.DEFAULT_LIMIT) {
        this.limit = initialLimit;
        // this.paginationLimitSelect = null;
        // $('.pagination_limit').load('pagination_limit.html', () => {
            // let btnLogout = document.querySelector('#select_pagination_limit');
            // $('.pagination-limit').load("components/pagination_limit.html", () => {
        //     this.paginationLimitSelect = document.getElementById('select_pagination_limit');
        //     console.log(this.paginationLimitSelect)
        //     this.paginationLimitSelect.addEventListener('change', () => this.handleChangePaginationLimit());
        // });
    }

    setLimit() {
        this.limit = parseInt(this.paginationLimitSelect.value);
    }

    getLimit() {
        return this.limit;
    }

    handleChangePaginationLimit() {
        this.setLimit();
        PaginationServices.setLimit(this.limit);
        $.fn.fetchPageRecords(1, this.limit);
    }
}
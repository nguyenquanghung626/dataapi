import DbDisplayServices from '../services/db_display_service.js';
import {HippoDbServices} from "../services/hippo_db_service.js";

let template = "";
const getTemplateListItemTable = function () {
    if (template != "") return template;
    return $.get("modal_table.html", function (data) {
        template = document.querySelector('.modalListTable');
        return template;
    });
}


const renderListItemTable = function (table, template) {
    template.querySelector(".grid-item-table__table-name").innerHTML = table.table_name;
    template.querySelector(".grid-item-table__content").innerHTML = "<a href='#' class='grid-item-table__content-link'>" + table.url + "</a>";
    template.querySelector(".grid-item-table__duration-value").innerHTML = table.duration;
    template.querySelector(".grid-item-table__table-name").setAttribute('data-table-name', table.table_name);
    const aTag = template.querySelector('.grid-item-table__content-link');
    const rootUrl = 'https://docs.google.com/spreadsheets/d/';
    aTag.href = rootUrl + table.url;
    aTag.target = '_blank';
    template.querySelector(".grid-item__created-at").innerHTML = "Create at: "+ table.created_at.split("T",1);
    return template;
}


const renderListTable = async (listTable) => {
    if (!listTable) return;
    $('.modalListTable .grid-list-table').empty();
    let template = await getTemplateListItemTable();
    let templateDom = $(template)[0];
    listTable.forEach(function (table) {
        let templateClone = templateDom.cloneNode(true);
        renderListItemTable(table, templateDom);
        let listItemDom = renderListItemTable(table, templateClone);
        $('.modalListTable ').append(templateClone);
    });
}


const fetchListAllTables = function () {
    HippoDbServices.listAllTable()
        .then(function (response) {
            let listTable = response;
            renderListTable(listTable);
        })
        .catch(function (error) {
            console.log(error);
        });
}


fetchListAllTables();

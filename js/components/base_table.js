export class BaseTable {

    constructor() {
        this.headers = [];
    }

    init() {
        this._renderHeader(this.headers);
        this._renderRows([])
    }

    init(headers) {
        this.headers = headers;
        this._renderHeader(headers);
        this._renderRows([])
    }

    renderRows(rows) {
        this._renderRows(rows);
    }

    //    setLoading(isLoading) {
    //        let loading = document.querySelector('#table-base__loading');
    //        if (isLoading) {
    //            document.querySelector('#table-base__body').innerHTML = '';
    //            loading.classList.remove('hidden');
    //
    //        } else {
    //            loading.classList.add('hidden');
    //
    //        }
    //    }

    _renderHeader = (headers) => {
        let headerEle = document.querySelector('#table-base__header');
        headerEle.innerHTML = '';

        headers.forEach(item => {
            let headerItem = document.createElement('th');
            headerItem.classList.add('header__item');
            headerItem.innerText = item;
            headerEle.appendChild(headerItem);
        })
    }

    _renderRows(rows) {
        let records = document.querySelector('#table-base__body');
        records.innerHTML = '';
        if (rows && rows.length > 0) {
            rows.forEach(row => {
                records.appendChild(this._createRowElement(row));
            })
        }
        if (rows && rows.length == 0) {
            records.innerHTML = '<tr><td colspan="14" class="text-center">No data</td></tr>';
        }
    }

    _createRowElement(row) {
        let record = document.createElement('tr');
        record.classList.add('record');
        let innerHTML = this.headers.map(header => {
            return `<td class="record__item">${row[header]}</td>`
        }).join('');
        record.innerHTML = innerHTML;
        return record;
    }
}
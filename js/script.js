import { setCookie } from './utils/cookies.js'
import TokenController from "./controllers/token_controller.js";
import JWT from "./lib/jwt.js";
import AuthController from './controllers/auth_controller.js';

let tokenController = TokenController.getInstance();
$(document).ready(() => {
    let promiseIsLogin = tokenController.checkStateLogin();
    promiseIsLogin.then((isLogin) => {
        if (isLogin) {
            return;
            // $('#login-form-container').load('logout', () => {
            //     let btnLogout = document.querySelector('#btn-logout');
            //     btnLogout.addEventListener('click', AuthController.handleLogout);
            // });
        } else {
            $('#login-form-container').load('login.html', () => {
                const loginForm = document.querySelector('#login-form');
                loginForm.addEventListener('submit', async(event) => {
                    event.preventDefault();
                    let form = new FormData(loginForm);
                    let a = await tokenController.fetchToken(form);
                    if (a == true)
                        window.location.href = 'data_table.html';
                    else
                        window.location.reload();
                })
            });
        }
    });
});
import DbDisplayConstants from './db_display_constants.js';
import '../lib/resp_pagination.js'
const PaginationServices = {
    initPagination,
    setLimit,
    updateOptions,
    updateSelectedValue,
    handleClickPageItem
}
export default PaginationServices;

$.max_value = 0;
$.selected_value = 0;
$.limit = DbDisplayConstants.DEFAULT_LIMIT;


function initPagination() {
    $("#pagination-container").append('<ul class="pagination"></ul>');
    $("#pagination-container").append("<br>");
    setLimit($.limit);
}

function updateSelectedValue(selected_value) {
    $.selected_value = selected_value;
    $(".pagination").generateRandomPagination({
        max_value: $.max_value,
        selected_value: $.selected_value
    });

    $(".pagination").rPage();
    return selected_value;
}

function setLimit(limit) {
    $.limit = limit;
    let max_value = Math.ceil(parseInt($('#total-rows').text()) / $.limit);
}

function updateOptions({ max_value, selected_value }) {
    $.max_value = max_value;
    $.selected_value = selected_value;
    $(".pagination").generateRandomPagination({
        max_value: max_value,
        selected_value: selected_value
    });
    $(".pagination").rPage();
}
$.fn.extend({
    "generateRandomPagination": function(options) {
        let { selected_value, max_value } = options;
        var content = '<li class="page-item left-etc pagination-previous" arial-label="previous"><a class="page-link" href="#">&laquo;</a></li>';
        let offset = Math.min(Math.min(Math.abs(selected_value - max_value), Math.abs(selected_value - 1)), 15);
        let left_bound = Math.max(selected_value - 30 + offset, 2);
        let right_bound = Math.min(selected_value + 30 - offset, options.max_value - 1);

        if (max_value > 0)
            content += `<li class="page-item ${selected_value==1}"><a class="page-link" href="#">${1}</a></li>`;
        for (var i = left_bound; i <= right_bound; i++) {
            if (i == selected_value)
                content += '<li class="page-item active"><a class="page-link" href="#">' + i + '</a></li>';
            else
                content += `<li class="page-item"><a class="page-link" href="#">${i}</a></li>`;
        }
        if (max_value > 1)
            content += `<li class="page-item ${selected_value==max_value?'active':''}"><a class="page-link" href="#">${max_value}</a></li>`;

        content += '<li class="page-item right-etc pagination-next" arial-label="next"><a class="page-link" href="#">&raquo;</a></li>';
        $(this).html(content);
        handleClickPageItem(() => $.fn.listenClickPageItem($.selected_value));
    }
});


//click paginations item
function handleClickPageItem(callback) {
    $(".page-item").click(function() {
        if ($(this).hasClass("active")) return;
        let page_number = $.selected_value;
        console.log($.selected_value)
        if ($(this).hasClass("pagination-previous")) {
            page_number = $.selected_value == 1 ? 1 : $.selected_value - 1;
        } else if ($(this).hasClass("pagination-next")) {
            page_number = $.selected_value == $.max_value ? $.selected_value : $.selected_value + 1;
        } else {
            page_number = parseInt($(this).find("a").text());
        }
        updateSelectedValue(page_number);
        callback();
    });
}
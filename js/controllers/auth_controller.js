import TokenController from "./token_controller.js";

const AuthController = {
    handleLogout: () => {
        if (!TokenController.getInstance().getAccessToken()) {
            return;
        }
        console.log(TokenController.getInstance().getAccessToken());
        TokenController.getInstance().cleanTokens();
        // console.log('Logout Successful');
        // window.location.href = 'index.html';
    },
};

export default AuthController;
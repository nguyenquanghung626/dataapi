import { API_ADDRESS } from "../config/API_ADDRESS.js";
import AuthController from "./auth_controller.js";
import TokenController from "./token_controller.js";

export {
    fetchData,
};

function handleAuthFailed({ url, method, body, isAuth }) {
    // Authen failed
    let refreshToken = TokenController.getInstance().getRefreshToken();
    if (!refreshToken) {
        AuthController.handleLogout();
        return;
    }
    return TokenController.getInstance().fetchAccessToken(refreshToken).then(response => {
        if (!response) {
            AuthController.handleLogout();
            return;
        }
        return fetchData(url, method, body, isAuth);
    }).catch(error => {
        console.log(error);
        AuthController.handleLogout();
    });
}

function fetchData(url, method="GET", body=null, isAuth=false) {
    let token = TokenController.getInstance().getAccessToken();
    let requestInit = {
        method: method,
        headers: {
            'Content-Type': 'application/json'
        }
    }
    if (method != 'GET') requestInit.body = JSON.stringify(body);
    if (isAuth) {
        let headers = requestInit.headers;
        let newHeaders = Object.assign(headers, {
            'Authorization': `Bearer ${token}`
        });
        requestInit.headers = newHeaders;
    }
    return fetch(url, requestInit)
    .then(response => {
        if (response.status == 401) {
            return handleAuthFailed({ url, method, body, isAuth });
        }
        return response.json();
    })
    .catch(error => {
        console.error(error);
    });
}
import { HippoDbServices } from "../services/hippo_db_service.js";
import { getCookie, setCookie, eraseCookie } from "../utils/cookies.js";
import JWT from "../lib/jwt.js";


// Token Controller is a class that handles the token
// Design Pattern: Singleton
var TokenController = (function () {
    var instance;

    function createInstance() {
        var _instance = {
            // Functions
            checkStateLogin: checkStateLogin,
            getAccessToken: getAccessToken,
            getRefreshToken,
            setAccessToken,
            setRefreshToken,
            cleanTokens,
            fetchToken,
            fetchAccessToken,
        };
        return _instance;
    }
    const setAccessToken = (token) => {
        localStorage.setItem('token', token);
    }
    const setRefreshToken = (token) => {
        setCookie('refresh', token);
    }
    const getAccessToken = () => {
        return localStorage.getItem('token');
    }
    const getRefreshToken = () => {
        return getCookie('refresh');
    }

    const checkStateLogin = async () => {
        if (!getCookie('refresh')) return false;
        let refreshToken = getRefreshToken();
        let resp = await HippoDbServices.getTokenFromRefresh(refreshToken)
        if (!resp) return false;
        let jwt = new JWT(resp.access);
        if (jwt.isExpired()) {
            this.eraseCookies();
            return false;
        }
        setAccessToken(resp.access);
        return true;
    }
    const fetchAccessToken = async (refresh_token) => {
        let resp = await HippoDbServices.getTokenFromRefresh(refresh_token)
        if (resp == null) return false;
        setAccessToken(resp.access);
        return true;
    }
    const fetchToken = async (body) => {
        let username = body.get('username');
        let password = body.get('password');
        let resp = await HippoDbServices.getToken(username, password);
        if (resp == null) return false;
        setAccessToken(resp.access);
        setRefreshToken(resp.refresh);
        return true;
    }
    const cleanTokens = () => {
        eraseCookie('refresh');
        localStorage.removeItem('token');
    }

    return {
        getInstance: function () {
            if (!instance) {
                instance = createInstance();
            }
            return instance;
        }
    };
})()

export default TokenController;
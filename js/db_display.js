import PaginationServices from './constants/paginations.js';
import { BaseTable } from './components/base_table.js';
import { debounce } from './utils/debounce.js';
import DbDisplayServices from './services/db_display_service.js';
import AuthController from './controllers/auth_controller.js'
import DbDisplayConstants from './constants/db_display_constants.js';

const tableNameInput = document.querySelector('#table_name_input');
const tableNameInputButton = document.querySelector('#table_name_input_button');
const searchTableNameInput = document.querySelector("#search_table_name_input")

$(document).ready(() => {
    $('#top_nav').load('top_nav.html', () => {
        let a = document.getElementById("alogout");
        a.addEventListener('click', AuthController.handleLogout);
    });
    $('.pagination_limit').load('pagination_limit.html', () => {
        let selectPaginationLimit = document.querySelector('#select_pagination_limit');
        selectPaginationLimit.addEventListener('change', () => {
            paginationLimit = parseInt(selectPaginationLimit.value);
            fetchPageRecords(1, paginationLimit);
        });
    });
    $('.modallisttable').load('modal_table.html', () => {
        $('body').append($('<script type="module" />').attr('src', '../js/components/modal_table.js'));
        $('head').append($('<link rel="stylesheet" type="text/css" />').attr('href', '../css/modal_list_table.css'));
    });
});


let baseTable = new BaseTable();
let tableName = "";
let fieldName = "id";
var listItems = [];
let page = 1;

PaginationServices.initPagination();
let paginationLimit = DbDisplayConstants.DEFAULT_LIMIT;
const totalRowsMatched = async function (value) {
    $('#total-rows').text(value);
    let nPages = Math.ceil(parseInt(value) / paginationLimit);
    let selected_value = 1
    PaginationServices.updateOptions({ max_value: nPages, selected_value: $.selected_value });
}
tableNameInput.value = "sims";
tableNameInputButton.addEventListener('click', () => { handleChangeTableName(tableName = tableNameInput.value); });
searchTableNameInput.addEventListener('input', (event) => handleInputSearchBar(event));

const handleInputSearchBar = async (event) => {
    debounce(async () => {
        let value = searchTableNameInput.value;
        if (value.length == 0) {
            fetchPageRecords();
            return;
        }
        let allRecordsResponses = await DbDisplayServices.fetchSearchWithField(
            tableName,
            fieldName,
            value,
            paginationLimit,
            DbDisplayConstants.DEFAULT_OFFSET
        );
        if (allRecordsResponses['status'] == "error") return;
        else {
            totalRowsMatched(allRecordsResponses["meta"]["total_rows"]);
            listItems = allRecordsResponses["data"]
            let response = await DbDisplayServices.fetchRecords(tableName, fieldName, listItems, paginationLimit, DbDisplayConstants.DEFAULT_OFFSET)
            baseTable.renderRows(response["data"]);
        }
    }, 100)();

}


const handleChangeTableName = async function (tableName) {

    let columnsResp = await DbDisplayServices.fetchColumns(tableName)
    if (!columnsResp || !columnsResp.data) return;
    let columns = columnsResp.data;
    let headers = columns.map(col => col.name);
    baseTable.init(headers);

    // Fetch total rows from resp select all
    let totalRowsResp = await DbDisplayServices.fetchTotalRows(tableName)
    if (totalRowsResp.status === 'success') {
        let total_rows = totalRowsResp['meta']['total_rows'];
        document.querySelector('#total-rows').innerText = total_rows;
        fetchPageRecords();
    }
}
let fetchPageRecords = async function (page = 1, limit = paginationLimit) {
    let offset = (page - 1) * paginationLimit + 1;
    let offset_records = DbDisplayConstants.DEFAULT_OFFSET;
    if (searchTableNameInput.value.length == 0) {
        let allRecordsResp = await DbDisplayServices.fetchSelectAll(tableName, paginationLimit, offset);
        if (allRecordsResp['status'] == 'error') return;
        else {
            let data = allRecordsResp['data'];
            baseTable.renderRows(data);
            totalRowsMatched(allRecordsResp['meta']['total_rows']);
        }
        return;
    }
    // check search input
    let res = await DbDisplayServices.fetchSearchWithField(tableName, fieldName, searchTableNameInput.value, limit, offset);
    if (res['status'] == "error") return;
    listItems = res["data"];
    totalRowsMatched(res["meta"]["total_rows"]);
    // // //check select fields
    let allReps = await DbDisplayServices.fetchRecords(tableName, fieldName, listItems, paginationLimit, offset_records);
    if (allReps["status" == "error"]) return;
    if (allReps['meta']['offset'] != offset_records) return;
    baseTable.renderRows(allReps["data"]);

};
$.fn.listenClickPageItem = fetchPageRecords;